import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:qldh/constants/color_constants.dart';

class AppConfig{
  static final AppConfig _appConfig=AppConfig._();
  factory AppConfig(){
    return _appConfig;
  }
  AppConfig._();

  Future<void> configApp() async{
    configLoading();
  }

  void configLoading() {
    EasyLoading.instance
      ..indicatorType = EasyLoadingIndicatorType.circle
      ..maskType = EasyLoadingMaskType.custom
      ..loadingStyle = EasyLoadingStyle.custom
      ..textColor = ColorConst.white
      ..indicatorSize = 40.0
      ..radius = 12.0
      ..backgroundColor = ColorConst.black.withOpacity(0.8)
      ..indicatorColor = ColorConst.white
      ..maskColor = ColorConst.black.withOpacity(0.2)
      ..userInteractions = false
      ..dismissOnTap = false;
  }
}