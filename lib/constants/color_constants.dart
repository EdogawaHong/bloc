import 'package:flutter/material.dart';

class ColorConst{
  static const Color primary=Colors.blueAccent;
  static const Color white=Colors.white;
  static const Color black=Colors.black;
}