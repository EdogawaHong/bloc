class TitlesConst{
  static const String home="Home title";
}

class ValidateConst{
  static const String username="Username is too short";
  static const String password="Password is too short";
}

class HintConst{
  static const String hintUser="Username";
  static const String hintPass="Password";
}