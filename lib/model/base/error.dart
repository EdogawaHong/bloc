class Error {
  int? code;
  String? systemMessage;
  String? message;

  Error({this.code, this.systemMessage, this.message});

  Error.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    systemMessage = json['system_message'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['system_message'] = this.systemMessage;
    data['message'] = this.message;
    return data;
  }
}