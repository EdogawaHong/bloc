import 'Error.dart';

class BaseAPI {
  Error? error;
  dynamic? data;

  BaseAPI({this.error, this.data});

  BaseAPI.fromJson(Map<String, dynamic> json) {
    error = json['error'] != null ? new Error.fromJson(json['error']) : null;
    data = json['data'];
  }
}
