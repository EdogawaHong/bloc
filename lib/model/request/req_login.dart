class RequestLogin {
  String? username;
  String? password;
  String? deviceId;
  String? fcmKey;
  String? channel;
  String? transid;

  RequestLogin(
      {this.username,
        this.password,
        this.deviceId,
        this.fcmKey,
        this.channel,
        this.transid});

  RequestLogin.fromJson(Map<String, dynamic> json) {
    username = json['username'];
    password = json['password'];
    deviceId = json['device_id'];
    fcmKey = json['fcm_key'];
    channel = json['channel'];
    transid = json['transid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['username'] = this.username;
    data['password'] = this.password;
    data['device_id'] = this.deviceId;
    data['fcm_key'] = this.fcmKey;
    data['channel'] = this.channel;
    data['transid'] = this.transid;
    return data;
  }
}