import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qldh/src/auth_repository.dart';
import 'package:qldh/src/form_submission_status.dart';
import 'package:qldh/src/login/login_bloc.dart';
import 'package:qldh/src/login/login_event.dart';
import 'package:qldh/src/login/login_state.dart';
import 'package:qldh/constants/text_constants.dart';

class LoginView extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
          create: (context) =>
              LoginBloc(authRepos: context.read<AuthRepository>()),
          child: _loginForm()),
    );
  }

  Widget _loginForm() {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        final formStatus = state.formStatus;
        if (formStatus is SubmissionFailed) {
          print("Error: ${formStatus.exception}");
        }
      },
      child: Form(
          key: _formKey,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 40),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _usernameField(),
                _passwordField(),
                _loginButton(),
              ],
            ),
          )),
    );
  }

  Widget _usernameField() {
    return BlocBuilder<LoginBloc, LoginState>(builder: (context, state) {
      return TextFormField(
        decoration: InputDecoration(
          icon: Icon(Icons.person),
          hintText: HintConst.hintUser,
        ),
        validator: (value) =>
            state.isValidUsername ? null : ValidateConst.username,
        onChanged: (value) => context
            .read<LoginBloc>()
            .add(LoginUsernameChanged(username: value)),
      );
    });
  }

  Widget _passwordField() {
    return BlocBuilder<LoginBloc, LoginState>(builder: (context, state) {
      return TextFormField(
        obscureText: true,
        decoration: InputDecoration(
          icon: Icon(Icons.security),
          hintText: HintConst.hintPass,
        ),
        validator: (value) =>
            state.isValidPassword ? null : ValidateConst.password,
        onChanged: (value) => context
            .read<LoginBloc>()
            .add(LoginPasswordChanged(password: value)),
      );
    });
  }

  Widget _loginButton() {
    return BlocBuilder<LoginBloc, LoginState>(builder: (context, state) {
      return state.formStatus is FormSubmitting
          ? CircularProgressIndicator()
          : ElevatedButton(
              onPressed: () {
                print("Username: ${state.username}");
                print("Password: ${state.password}");
                 if(_formKey.currentState!.validate()){
                context.read<LoginBloc>().add(LoginSubmitted());
                }
              },
              child: Text('login'));
    });
  }
}
