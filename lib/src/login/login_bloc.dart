import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qldh/src/auth_repository.dart';
import 'package:qldh/src/form_submission_status.dart';
import 'package:qldh/src/login/login_event.dart';
import 'package:qldh/src/login/login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final AuthRepository authRepos;

  LoginBloc({required this.authRepos}) : super(LoginState());

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginUsernameChanged) {
      yield state.copyWith(username: event.username);
    } else if (event is LoginPasswordChanged) {
      yield state.copyWith(password: event.password);
    } else if (event is LoginSubmitted) {
      yield state.copyWith(formStatus: FormSubmitting());
      try {
        await authRepos.login();
        yield state.copyWith(formStatus: SubmissionSuccess());
      } catch (e) {
        yield state.copyWith(
            formStatus: SubmissionFailed(exception: e.toString()));
      }
    }
  }
}
