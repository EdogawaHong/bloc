import 'dart:io';

import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:qldh/commons/utils/shared_pref.dart';
import 'package:qldh/config/constance.dart';

class NetworkUtils {
  static bool dev = true;
  static String BASE_URL_DEV = 'http://103.147.34.56:9036/btt/qldh/api/';

  static Future<String> getTOKEN() async {
    return await SharedPref.getString(Constance.token);
  }

  static String getBaseUrl() {
    return BASE_URL_DEV;
  }

  static Dio getDioClientToken() {
    String token = Constance.tokenKey;
    getTOKEN().then((tokenSaved) {
      token = tokenSaved;
    });
    print("Token Request = $token");
    final dio = Dio(
        BaseOptions(baseUrl: getBaseUrl(), contentType: "application/json"));
    dio.options.headers["Authorization"] = "Bearer $token";
    dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: true,
        error: true,
        compact: true));
    return dio;
  }

  static Dio getDioClientWithoutToken() {
    final dio = Dio(BaseOptions(
        baseUrl: getBaseUrl(),
        contentType: "application/json")); // Provide a dio instance
    dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: true,
        error: true,
        compact: true));
    return dio;
  }

  static Future<bool> hasConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        return true;
      } else {
        return false;
      }
    } on SocketException catch (_) {
      print('not connected');
      return false;
    }
  }
}

abstract class ErrorExpired {
  void expiredTime();
}