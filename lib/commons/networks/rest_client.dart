import 'package:dio/dio.dart';
import 'package:qldh/model/base/base_api.dart';
import 'package:qldh/model/request/req_login.dart';
import 'package:retrofit/retrofit.dart';

part 'rest_client.g.dart';

@RestApi()
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @POST("app/account/login")
  Future<BaseAPI> login(@Body() RequestLogin login);
}